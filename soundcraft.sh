#!/bin/bash
alsa_input="alsa_input.usb-Soundcraft_Soundcraft_Signature_22_MTK-00.multichannel-input"
alsa_input_channels=(front-left front-right rear-left rear-right front-center lfe side-left side-right aux0 aux1 aux2 aux3 aux4 aux5 aux6 aux7 aux8 aux9 aux10 aux11 aux12 aux13 aux14 aux15)
alsa_input_length="$((${#alsa_input_channels[*]} - 2))"

alsa_output="alsa_output.usb-Soundcraft_Soundcraft_Signature_22_MTK-00.multichannel-output"
alsa_output_channels=(front-left front-right rear-left rear-right front-center lfe side-left side-right aux0 aux1 aux2 aux3 aux4 aux5 aux6 aux7 aux8 aux9 aux10 aux11 aux12 aux13)
alsa_output_length="$((${#alsa_output_channels[*]} - 2))"

device_name="Soundcraft Signature 22 MTK"

echo "${#alsa_input_channels[*]} inputs"
echo "${#alsa_output_channels[*]} outputs"

mixer_state="100"
function load_mixer {
    pactl unload-module module-remap-source
    pactl unload-module module-remap-sink
    
    for i in $(eval echo {0..$alsa_input_length..2})
    do
        channels="${alsa_input_channels[$i]},${alsa_input_channels[$i+1]}"
        name="$device_name $(($i + 1))-$(($i + 2))"
        pactl load-module module-remap-source source_name="\"$name\"" source_properties="\"device.description='$name' device.icon_name='audio-card-usb'\"" master="$alsa_input" master_channel_map="$channels" channel_map=front-left,front-right
    done
    
    for i in $(eval echo {0..$alsa_output_length..2})
    do
        channels="${alsa_output_channels[$i]},${alsa_output_channels[$i+1]}"
        name="$device_name $(($i + 1))-$(($i + 2))"
        pactl load-module module-remap-sink sink_name="\"$name\"" sink_properties="\"device.description='$name' device.icon_name='audio-card-usb'\"" master="$alsa_output" master_channel_map="$channels" channel_map=front-left,front-right
    done
}

function unload_mixer {
    pactl unload-module module-remap-source
    pactl unload-module module-remap-sink
}

while [ 1 ]
do
    USB_STATE=$(lsusb | grep "05fc:0022" | wc -l)
    echo $USB_STATE
    echo $mixer_state
    if [ "$USB_STATE" == "1" ] && [ "$mixer_state" == "100" ]; then
        load_mixer
        mixer_state=101
        echo "Loaded mixer"
        elif [ "$USB_STATE" == "0" ] && [ "$mixer_state" == "101" ]; then
        unload_mixer
        mixer_state=100
        echo "Unloaded mixer"
    else
        echo "Nothing to do"
    fi
    sleep 2
done
